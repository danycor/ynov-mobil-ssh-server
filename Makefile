up:
	docker-compsoe up -d

down:
	docker-compose down

build:
	docker-compose build

autodeploy: down build
	git pull origin master && docker-compose up -d